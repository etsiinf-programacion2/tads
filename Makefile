# Sello de versión
VSN=20230613

JAR=tads-$(VSN).jar
DOCZIP=tads-doc-$(VSN).zip
SRCZIP=tads-src-$(VSN).zip

default: $(JAR) $(DOCZIP) $(SRCZIP)

# Directorios
SRC=src
BIN=bin
DOC=doc
TEST=tests

$(DOC):
	javadoc -d $@ -tag inv.:c:"Invariant: " -tag pre.:a:"Precondition: " -tag post.:a:"Postcondition: " -encoding UTF-8 -charset UTF-8 -docencoding UTF-8 -sourcepath $(SRC) tads

$(BIN):
	javac -d $@ -sourcepath $(SRC) $(SRC)/tads/*.java

$(JAR): $(BIN)
	jar cvf $@ -C $^ tads

$(DOCZIP): $(DOC)
	zip -r $@ $^

$(SRCZIP):
	zip -r $@ $(SRC)

test: $(JAR)
	for t in TestQueue TestStack TestList; do \
		cd $(TEST); \
		javac -cp .:../$(JAR) $$t.java && \
		timeout -s KILL 3s java -cp .:../$(JAR) -ea $$t; \
		cd ..; \
	done;

clean:
	rm -rf $(DOC) $(BIN)

veryclean: clean
	rm -f $(JAR) $(DOCZIP)

.PHONY: $(DOC) $(BIN) clean veryclean test
