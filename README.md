# Bibliotecas usadas en la asignatura Programación 2 en la UPM

Algunas bibliotecas son propias y otras son recursos de otras universidades o desarrolladores.

## Biblioteca de TADs (tipos abstractos de datos)

Bibliotecas principal de la asignatura Programación 2 en la
UPM. Contiene la implementación de los TADs vistos en la asignatura.

Para compilar, construir la documentación o el .jar mejor mirar el fichero Makefile.
