import tads.*;

public class TestStack {
  private static final int MAX = 1000;

  public static void main (String[] args) {
    System.out.println("Probando LinkedStack");
    IStack<Integer> stack = new LinkedStack<>();
    test(stack);

    System.out.println("Probando ArrayStack");
    IStack<Integer> arrayStack = new ArrayStack<>();
    test(arrayStack);

    System.out.println("Probando BoundedStack");
    IBoundedStack<Integer> boundedStack = new BoundedStack<>(MAX+1);
    test(boundedStack);
    testFull(boundedStack);
  }

  private static void test (IStack<Integer> stack) {
    assert stack.isEmpty() : "Inicialmente tiene que estar vacía";

    try {
      stack.pop();
      assert false : "Pop sobre una pila vacía debería elevar una excepción";
    }
    catch (IllegalArgumentException e) {
    }
    catch (Exception e) {
      assert false : "Pop sobre una pila vacía debería elevar una IllegarlArgumentException";
    }

    try {
      stack.peek();
      assert false : "Peek sobre una pila vacía debería elevar una excepción";
    }
    catch (IllegalArgumentException e) {
    }
    catch (Exception e) {
      assert false : "Peek sobre una pila vacía debería elevar una IllegarlArgumentException";
    }

    stack.push(42);
    assert !stack.isEmpty() : "Tiene un elemento, no puede estar vacía";

    assert stack.peek() == 42 : "El elemento tiene que ser 42";
    stack.pop();
    assert stack.isEmpty() : "Ya no tiene el 42, tiene que estar vacía";

    for (int i = 0; i < MAX; ++i) {
      stack.push(MAX - i);
    }

    for (int i = 1; i < MAX; ++i) {
      assert !stack.isEmpty() : "No puede estar vacía";
      assert stack.peek() == i : "El elemento esperado es " + i;
      stack.pop();
    }

    assert !stack.isEmpty() : "No está vacía";
    assert stack.peek() == MAX : "El elemento esperado es " + MAX;

    for (int i = 0; i < MAX; ++i) {
      stack.push(MAX - i);
    }

    assert stack.peek() == 1 : "El elemento tiene que ser 1";
    assert !stack.isEmpty() : "No puede estar vacía";
    stack.pop();

    for (int i = 2; i < MAX; ++i) {
      assert !stack.isEmpty() : "No puede estar vacía, quedan " + (MAX - i) + " elementos";
      assert stack.peek() == i : "El elemento tiene que ser " + i;
      stack.pop();
    }

    assert !stack.isEmpty() : "No puede estar vacía";
    assert stack.peek() == MAX : "El elemento tiene que ser " + MAX;
    stack.pop();

    assert !stack.isEmpty() : "No puede estar vacía";
    assert stack.peek() == MAX : "El elemento tiene que ser " + MAX;
    stack.pop();

    assert stack.isEmpty() : "Tiene que estar vacía";

    try {
      stack.peek();
    } catch (RuntimeException e) {
      return;
    }
    assert false : "peek debería producir una excepción si está vacía";
  }

  private static void testFull (IBoundedStack<Integer> stack) {
    while (!stack.isFull()) {
      stack.push(33);
    }
    try {
      stack.push(22);
    } catch (IllegalArgumentException e) {
      return;
    }
    assert false : "push debería producir una excepción si está llena";
  }
}
