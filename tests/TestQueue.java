import tads.*;

public class TestQueue {

  static int[] datos = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

  public static void main (String[] args) {
    System.out.println("Probando CircularBufferQueue");
    IBoundedQueue<Integer> circularQueue = new CircularBufferQueue<>();
    test(circularQueue);
    testFull(circularQueue);

    System.out.println("Probando BoundedQueue");
    IBoundedQueue<Integer> boundedQueue = new BoundedQueue<>();
    test(boundedQueue);
    testFull(boundedQueue);

    System.out.println("Probando LinkedQueue");
    IQueue<Integer> queue = new LinkedQueue<>();
    test(queue);

    System.out.println("Probando ArrayQueue");
    IQueue<Integer> arrayQueue = new ArrayQueue<>();
    test(arrayQueue);
  }

  private static void test (IQueue<Integer> queue) {
    assert queue.isEmpty() : "Inicialmente tiene que estar vacía";

    try {
      queue.poll();
      assert false : "Poll sobre una cola vacía debería elevar una excepción";
    }
    catch (IllegalArgumentException e) {
    }
    catch (Exception e) {
      assert false : "Poll sobre una cola vacía debería elevar una IllegarlArgumentException";
    }

    try {
      queue.peek();
      assert false : "Peek sobre una cola vacía debería elevar una excepción";
    }
    catch (IllegalArgumentException e) {
    }
    catch (Exception e) {
      assert false : "Peek sobre una cola vacía debería elevar una IllegarlArgumentException";
    }

    queue.add(44);
    assert !queue.isEmpty() : "Tiene un elemento, no puede estar vacía";

    assert queue.peek() == 44 : "El elemento tiene que ser 44";
    queue.poll();
    assert queue.isEmpty() : "Ya no tiene el 44, tiene que estar vacía";

    for (int i = 0; i < datos.length; ++i) {
      queue.add(datos[i]);
    }

    for (int i = 0; i < datos.length - 1; ++i) {
      assert queue.peek() == datos[i] : "El elemento esperado es " + datos[i];
      assert !queue.isEmpty() : "No puede estar vacía";
      queue.poll();
    }

    assert !queue.isEmpty() : "No está vacía";

    for (int i = 0; i < datos.length; ++i) {
      queue.add(datos[i]);
    }

    assert queue.peek() == datos[datos.length - 1] : "El elemento tiene que ser " + datos[datos.length - 1];
    assert !queue.isEmpty() : "No puede estar vacía";
    queue.poll();

    for (int i = 0; i < datos.length; ++i) {
      assert queue.peek() == datos[i] : "El elemento tiene que ser " + datos[i];
      assert !queue.isEmpty() : "No puede estar vacía, quedan " + (datos.length - i) + " elementos";
      queue.poll();
    }

    assert queue.isEmpty() : "Tiene que estar vacía";

    try {
      queue.peek();
    } catch (RuntimeException e) {
      return;
    }
    assert false : "peek debería producir una excepción si está vacía";
  }

  private static void testFull (IBoundedQueue<Integer> queue) {
    while (!queue.isFull()) {
      queue.add(33);
    }
    try {
      queue.add(22);
    } catch (RuntimeException e) {
      return;
    }
    assert false : "push debería producir una excepción si está llena";
  }
}
