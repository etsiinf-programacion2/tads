package tads;

import java.util.Arrays;

/**
 * Implementación de {@code IQueue} mediante un array redimensionable.
 */
public class ArrayQueue<E> implements IQueue<E> {
  private static final int MIN_LENGTH = 1024;
  private Object[] elements;
  private int last; // Indica la posición del último elemento insertado

  public ArrayQueue () {
    elements = new Object[MIN_LENGTH];
    last = -1;
  }

  public ArrayQueue (int n) {
    elements = new Object[n];
    last = -1;
  }

  public boolean isEmpty () {
    return last == -1;
  }

  public void add (E element) {
    if ((last + 1) == elements.length) {
      elements = Arrays.copyOf(elements, 2 * elements.length);
    }
    last = last + 1;
    elements[last] = element;
  }

  public E peek () {
    if (isEmpty()) {
      throw new IllegalArgumentException("peek: is empty.");
    } else {
      @SuppressWarnings("unchecked")
      E result = (E) elements[0];
      return result;
    }
  }

  public void poll () {
    if (isEmpty()) {
      throw new IllegalArgumentException("poll: is empty.");
    } else {
      for (int i = 0; i <= last - 1; i++) {
        elements[i] = elements[i + 1];
      }
      last = last - 1;
    if (last < elements.length / 2 && elements.length > MIN_LENGTH)
      elements = Arrays.copyOf(elements, elements.length / 2);
    }
  }

  public String toString () {
    String result = "[";
    for (int i = 0; i <= last; i++) {
      result += elements[i];
      if (i != last)
        result += ", ";
    }
    return result + "]";
  }

}
