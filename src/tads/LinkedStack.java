package tads;

/**
 * Implementación de {@code IQueue} mediante una cadena enlazada.
 */
public class LinkedStack<E> implements IStack<E> {
  private Node<E> top;

  public LinkedStack () {
    top = null;
  }

  public boolean isEmpty () {
    return top == null;
  }

  public void push (E element) {
    top = new Node<>(element, top);
  }

  public void pop () {
    if (isEmpty())
      throw new IllegalArgumentException("Pop an empty stack");
    top = top.next;
  }

  public E peek () {
    if (isEmpty())
      throw new IllegalArgumentException("Peek an empty stack");
    return top.element;
  }

  public String toString () {
    String result = "[";
    Node<E> iter = top;
    while (iter != null) {
      result += iter.element;
      if (iter.next != null)
        result += ", ";
      iter = iter.next;
    }
    result += "]";
    return result;
  }
}
