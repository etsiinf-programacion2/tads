package tads;

/**
 * Añade el método {@code isFull} a {@code IStack}.
 *
 * El método {@code add} podría lanzar una {@code IllegalArgumentException}
 * si la pila está llena.
 */
public interface IBoundedStack<E> extends IStack<E> {
  /**
   * Comprueba si la pila está llena.  
   *
   * @post. Cuando el {@code resultado} es {@code false} se puede insertar
   * un nuevo elemento en la pila y viceversa. 
   * @return verdadero si la pila está llena y falso en caso contrario
   */
  boolean isFull ();
}
