package tads;

/**
 * Especificación del TAD {@code IQueue}: las colas (queue) son
 * colecciones de datos FIFO, <em>first-in-first-out</em> (se ha
 * tomado el modelo el interfaz de la clase {@code Queue} de Java).
 */
public interface IQueue<E> {

  /**
   * Comprueba si la cola está vacía.
   *
   * @return verdadero si la cola está vacía, falso en caso contrario
   */
  boolean isEmpty ();

  /**
   * Inserta un nuevo elemento al final de la cola.
   *
   * @param valor el nuevo valor insertado
   * @pre. la cola no puede estar llena
   * @post. La cola tiene un elemento más al final
   */
  void add (E valor);

  /**
   * Devuelve el primer elemento de la cola.
   *
   * @pre. la cola no puede estar vacía
   * @post. la cola no se modifica
   * @return el primer elemento de la cola
   * @throws IllegalArgumentException si la cola está vacía.
   */
  E peek ();

  /**
   * Elimina el primer elemento de la cola
   *
   * @pre. la cola no puede estar vacía
   * @post. la cola tiene un elemento menos
   * @throws IllegalArgumentException si la cola está vacía.
   */
  void poll ();

  /**
   * Devuelve una representación de la cola. En el string devuelto,
   * <b>el elemento que lleva más tiempo en la cola debería ser el
   * primer elemento en aparecer</b>.
   */
  String toString();
}
