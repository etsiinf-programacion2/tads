package tads;

/**
 * Implementación de {@code IBoundedQueue} mediante un array circular.
 */
public class CircularBufferQueue<E> implements IBoundedQueue<E> {
  private static final int DEFAULT_MAX_SIZE = 1024;
  private Object[] buffer;
  private int size = 0;
  // turnAround == last < first
  private boolean turnAround = false;
  // Indexes of element in the circular queue are located in [ first, last ) when turnArround is false
  // or in [ first, buffer.lenght ) and [ 0, last ) when turnArround is true
  private int first = 0;
  private int last = 0;

  public CircularBufferQueue () {
    // Note: it is not possible to do: new E[DEFAULT_MAX_SIZE]
    buffer = new Object[DEFAULT_MAX_SIZE];
  }

  public CircularBufferQueue (int n) {
    buffer = new Object[n];
  }

  public boolean isEmpty () {
    return size == 0;
  }

  public boolean isFull () {
    return size >= buffer.length;
  }

  public void add (E value) {
    if (size + 1 > buffer.length) {
      throw new IllegalArgumentException("add: is full");
    }

    if (isEmpty()) {
      turnAround = false;
      first = 0;
      last = 0;
    } else if (!turnAround && last >= buffer.length && first > 0) {
      turnAround = true;
      last = 0;
    } else if (!turnAround && last >= buffer.length && first == 0) {
      // Sanity check: full queue should be detected at first if
      throw new RuntimeException("add: SHOULD NOT BE POSSIBLE TI GET HERE");
    } else if (turnAround && last >= first) {
      // Sanity check: full queue should be detected at first if
      throw new RuntimeException("add: SHOULD NOT BE POSSIBLE TI GET HERE");
    }
    buffer[last] = value;
    last++;
    size++;
  }

  public E peek () {
    if (isEmpty()) {
      throw new IllegalArgumentException("peek: is empty");
    }
    // Unchecked conversion, it is all rigth since all elements are E
    @SuppressWarnings("unchecked")
    E result = (E) buffer[first];
    return result;
  }

  public void poll () {
    if (isEmpty()) {
      throw new IllegalArgumentException("poll: is empty");
    }

    first++;
    if (first >= buffer.length) {
      first = 0;
      turnAround = false;
    }
    size--;
  }

  @Override
  public String toString () {
    StringBuilder result = new StringBuilder();
    result.append(String.format("(size %d):", size));
    result.append("[");
    if (size > 0) {
      result.append(buffer[first]);
    }
    for (int i = 1; i < size; ++i) {
      result.append(", ");
      result.append(buffer[(i + first) % buffer.length]);
    }
    result.append("]");
    return result.toString();
  }

}
