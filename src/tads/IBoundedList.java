package tads;

/**
 * Añade el método {@code isFull} a {@code IList} y los métodos {@code
 * add} pueden elevar excepciones para indicar que han sido llamados
 * sin respetar la precondición de no estar llena.
 */
public interface IBoundedList<E> extends IList<E> {
  /**
   * Comprueba si la lista está llena.
   *
   * @post. no modifica la lista
   * @return verdadero si la lista está llena, falso en caso contrario
   */
  boolean isFull ();
}
