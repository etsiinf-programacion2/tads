package tads;

/**
 * Implementación de {@code IBoundedStack} mediante un array.
 */
public class BoundedStack<E> implements IBoundedStack<E> {
  private static final int DEFAULT_LENGTH = 1024;
  private Object[] elements;
  private int size;

  public BoundedStack () {
    size = 0;
    elements = new Object[DEFAULT_LENGTH];
  }

  public BoundedStack (int n) {
    size = 0;
    elements = new Object[n];
  }

  public boolean isEmpty () {
    return size == 0;
  }

  @SuppressWarnings("unchecked")
  public E peek () {
    if (isEmpty())
      throw new IllegalArgumentException("peek: is empty");
    return (E) elements[size - 1];
  }

  public void pop () {
    if (isEmpty())
      throw new IllegalArgumentException("pop: is empty");
    if (size >= 1) {
      size = size - 1;
    }
  }

  public boolean isFull () {
    return size >= elements.length;
  }

  public void push (E element) {
    if (isFull())
      throw new IllegalArgumentException("push: is full");
    else {
      elements[size] = element;
      size = size + 1;
    }
  }

  public String toString () {
    String result = "[";
    for (int i = size - 1; i >= 0; i--) {
      result += elements[i];
      if (i != 0)
        result += ", ";
    }
    return result + "]";
  }
}
