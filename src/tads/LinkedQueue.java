package tads;

/**
 * Implementación de {@code IQueue} mediante una cadena enlazada con
 * puntero al primero y al último.
 */
public class LinkedQueue<E> implements IQueue<E> {
  private Node<E> first = null;
  private Node<E> last = null;

  public boolean isEmpty () {
    return first == null;
  }

  public void add (E valor) {
    if (isEmpty()) {
      first = new Node<>(valor, null);
      last = first;
    } else {
      last.next = new Node<>(valor, null);
      last = last.next;
    }
  }

  public E peek () {
    if (isEmpty())
      throw new IllegalArgumentException("Peek an empty queue");
    return first.element;
  }

  public void poll () {
    if (isEmpty())
      throw new IllegalArgumentException("Poll an empty queue");
    first = first.next;
  }

  @Override
  public String toString () {
    StringBuilder result = new StringBuilder();
    result.append("[");
    Node<E> iter = first;
    while (iter != null) {
      result.append(iter.element);
      if (iter.next != null) {
        result.append(", ");
      }
      iter = iter.next;
    }
    result.append("]");
    return result.toString();
  }

}
