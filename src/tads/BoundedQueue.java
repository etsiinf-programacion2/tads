package tads;

/**
 * Implementación de {@code IBoundedQueue} mediante un array.
 */
public class BoundedQueue<E> implements IBoundedQueue<E> {
  private static final int DEFAULT_MAX_SIZE = 1024;
  private Object[] elements;
  private int last; // Indica la posición del último elemento insertado

  public BoundedQueue () {
    elements = new Object[DEFAULT_MAX_SIZE];
    last = -1;
  }

  public BoundedQueue (int n) {
    elements = new Object[n];
    last = -1;
  }

  public boolean isEmpty () {
    return last == -1;
  }

  public boolean isFull () {
    return (last + 1) == elements.length;
  }

  public void add (E element) {
    if (!isFull()) {
      last = last + 1;
      elements[last] = element;
    } else {
      throw new IllegalArgumentException("add: is full.");
    }
  }

  public E peek () {
    if (isEmpty()) {
      throw new IllegalArgumentException("peek: is empty.");
    } else {
      @SuppressWarnings("unchecked")
      E result = (E) elements[0];
      return result;
    }
  }

  public void poll () {
    if (isEmpty()) {
      throw new IllegalArgumentException("poll: is empty.");
    } else {
      for (int i = 0; i <= last - 1; i++) {
        elements[i] = elements[i + 1];
      }
      last = last - 1;
    }
  }

  public String toString () {
    String result = "[";
    for (int i = 0; i <= last; i++) {
      result += elements[i];
      if (i != last)
        result += ", ";
    }
    return result + "]";
  }

}
