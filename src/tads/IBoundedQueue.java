package tads;

/**
 * Añade el método {@code isFull} a {@code IQueue}.
 *
 * El método {@code add} podría lanzar una {@code IllegalArgumentException}
 * si la cola está llena.
 */
public interface IBoundedQueue<E> extends IQueue<E> {
  /**
   * Comprueba si la cola está llena.
   *
   * @return verdadero si la cola está llena, falso en caso contrario
   */
  boolean isFull ();
}
