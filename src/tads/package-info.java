/**
 * El paquete {@code tads} es un paquete con <b>tipos abstractos de
 * datos <em>lineales</em></b> y las <b>estructuras de datos</b> que
 * los implementan usado en la asignatura de <b>Programación 2</b> de la
 * <b>Universidad Politécnica de Madrid</b>.
 */
package tads;
