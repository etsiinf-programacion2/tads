package tads;

import java.util.Arrays;

/**
 * Implementación de {@code IStack} mediante un array redimensionable.
 */
public class ArrayStack<E> implements IStack<E> {
  private static final int MIN_LENGTH = 1024;
  private Object[] elements;
  private int size;

  public ArrayStack () {
    size = 0;
    elements = new Object[MIN_LENGTH];
  }

  public boolean isEmpty () {
    return size == 0;
  }

  public E peek () {
    if (isEmpty())
      throw new IllegalArgumentException("peek: is empty");

    @SuppressWarnings("unchecked")
    E result = (E) elements[size - 1];
    return result;
  }

  public void pop () {
    if (isEmpty())
      throw new IllegalArgumentException("pop: is empty stack");

    if (size >= 1) {
      size = size - 1;
    }
    if (size < elements.length / 2 && elements.length > MIN_LENGTH)
      elements = Arrays.copyOf(elements, elements.length / 2);
  }

  public void push (E element) {
    if (size == elements.length)
      elements = Arrays.copyOf(elements, 2 * elements.length);
    elements[size] = element;
    size = size + 1;
  }

  public String toString () {
    String result = "[";
    for (int i = size - 1; i >= 0; i--) {
      result += elements[i];
      if (i != 0)
        result += ", ";
    }
    return result + "]";
  }
}
