package tads;

/**
 * Especificación del TAD {@code IStack}: las pilas (stack) son
 * colecciones de datos LIFO, <em>last-in-first-out</em> (se ha tomado
 * el modelo el interfaz de la clase {@code Stack} de Java).
 */
public interface IStack<E> {
  /**
   * Comprueba si la pila está vacía.
   *
   * @post. {@code resultado} es {@code true} si la pila está vacía y
   *        {@code false} en caso contrario.
   * @return verdadero si la pila está vacía, falso en caso contrario
   */
  boolean isEmpty ();

  /**
   * Devuelve el elemento que está en la cima de la pila sin eliminarlo y, por tanto,
   * sin modificar la pila.
   *
   * @pre. La pila no esta vacía.
   * @post. {@code resultado} es el elemento que está en la cima de la pila, la
   * pila conserva el mismo número de elementos.
   * @return el elemento que está en la cima de la pila.
   * @throws IllegalArgumentException si la pila está vacía (depende de la implementación).
   */
  E peek ();

  /**
   * Borra la cima de la pila.
   * Si la pila está vacía, no hace nada.
   *
   * @pre. La pila no esta vacía.
   * @post. la pila tiene un elemento menos, su cima.
   * @throws IllegalArgumentException si la pila está vacía (depende de la implementación).
   */
  void pop ();

  /**
   * Añade el elemento {@code e} a la cima de la pila.
   *
   * @param e el elemento que se añade a la pila.
   * @pre. La pila no está llena.
   * @post. La pila tiene un elemento más.
   */
  void push (E e);

  /**
   * Devuelve una representación de la pila. En el string devuelto,
   * <b>la cima de la pila debería ser el primer elemento en
   * aparecer</b>.
   */
  String toString();
}
